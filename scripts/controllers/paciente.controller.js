(function () {
    'use strict';
    angular.module('app')
        .controller('pacienteCtrl', ['$scope', '$http', '$rootScope', '$location','PacienteService', pacienteCtrl])        

    function pacienteCtrl($scope, $http, $rootScope, $location, PacienteService) {        
        
        $scope.init = function(){
            $scope.service = new PacienteService($scope);
            $scope.getPaciente();         
           
        }

        $scope.getPaciente = function(){
            $scope.service.getPacientes(function (result){ 
                $scope.pacientes = result;
            })
            
        }


        $scope.salvar = function(){
            alert('salvo')
        }

        $scope.goTo = function(paciente) { 
            $location.path('paciente-detalhe/' + paciente.id);
        };

       
       

        $scope.init();
        
    };


})(); 