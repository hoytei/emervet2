 (function () {
    'use strict';
    angular.module('app')
        .config(function($routeProvider) { 
            $routeProvider 
            .when("/", {
                templateUrl : "views/dashboard.html"
            })  
            .when("/clientes", {
                templateUrl : "views/clientes/lista-clientes.html"
            })      
            .when("/cliente-detalhe", {
                templateUrl : "views/clientes/clientes-detalhe.html"
            })
            .when("/cliente-detalhe/:ID", {
                templateUrl : "views/clientes/clientes-detalhe.html"
            })
            .when("/pacientes", {
                templateUrl : "views/pacientes/pacientes-lista.html"
            })
            .when("/paciente-detalhe/:ID", {
                templateUrl : "views/pacientes/pacientes-detalhe.html"
            })
            .when("/medicamentos", {
                templateUrl : "views/medicamentos/medicamentos-lista.html"
            })
            .when("/medicamento-detalhe/", {
                templateUrl : "views/medicamentos/medicamento-detail.html"
            })
            .when("/medicamento-detalhe/:ID", {
                templateUrl : "views/medicamentos/medicamento-detail.html"
            })
            .when("/calculadora", {
                templateUrl : "views/calculadora.html"
            });
           
        });
})();