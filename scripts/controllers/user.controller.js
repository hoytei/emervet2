(function () {
    'use strict';
    angular.module('app')
        .controller('userCtrl', ['$scope', '$http', '$rootScope', '$location','ClienteService', userCtrl])        

    function userCtrl($scope, $http, $rootScope, $location, ClienteService) {        
        
        $scope.init = function(){
            $scope.service = new ClienteService($scope);
            $scope.getCliente();
          
           
        }

        $scope.getCliente = function(){
            $scope.service.getClientes(function (result){ 
               $scope.clientes = result;
            })
           
        }


        $scope.salvar = function(){
            alert('salvo')
        }

        $scope.goTo = function(cliente) {
            $location.path('cliente-detalhe/' + cliente.id);
        };

       
       

        $scope.init();
        
    };


})(); 