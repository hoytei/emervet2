(function () {
    'use strict';
    angular.module('app')
        .controller('signInCtrl', ['$scope', '$http', '$rootScope', 'SignInService','$window', signInCtrl])        

    function signInCtrl($scope, $http, $rootScope, SignInService,$window) {        
        
        $scope.init = function(){
             $scope.signInService = new SignInService($scope);
             $scope.loading = false;
             $rootScope.showHeader = false;
             $window.localStorage.removeItem('token');
        }

        $scope.verificaSeNaoFoiPreenchido = function(variavel){ 
                if(variavel == null || variavel == '')
                    return true
                
                return false
        }

        $scope.verificarDados = function(){ 
           $scope.empty_name = false;
           $scope.empty_email = false;
           $scope.empty_pass = false;
           $scope.empty_repeat_pass = false;
            $scope.different_pass = false;

           if($scope.verificaSeNaoFoiPreenchido($scope.nome)){
                $scope.autorizaSignIn = false;
                $scope.empty_name = true;
           }else if($scope.verificaSeNaoFoiPreenchido($scope.email)){
                $scope.autorizaSignIn = false;
                $scope.empty_email = true;
           }else if($scope.verificaSeNaoFoiPreenchido($scope.senha)){
                $scope.autorizaSignIn = false;
                $scope.empty_pass = true;
           }else if($scope.verificaSeNaoFoiPreenchido($scope.confirma_senha) || $scope.senha != $scope.confirma_senha){ 
                $scope.autorizaSignIn = false;
                $scope.empty_repeat_pass = true;
           }else{
                $scope.autorizaSignIn = true;
           }

           if($scope.senha != $scope.confirma_senha){
            $scope.different_pass = true;
           }

           if($scope.autorizaSignIn){ 
                $scope.signIn();
           }

        }

        $scope.signIn = function(){            
            $scope.userJson = {
                                name:  $scope.nome,
                                password: $scope.senha,
                                email: $scope.email,
                                password_confirmation : $scope.confirma_senha

            }
             $scope.loading = true;

            $scope.signInService.save($scope.userJson, function (result){
              if(result.status != 200){ 
                 $scope.loading = false;                 
                 window.location ="#/dashboard"
              }
              alert(result.data.message)
               var user_token = result.data.data.token;
                $window.localStorage.setItem('token',user_token)
                window.location ="#/dashboard"

            });
            
        }
       


        $scope.init();
        
    };


})(); 