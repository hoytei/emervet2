(function () {
    'use strict';
    angular.module('app')
        .controller('loginCtrl', ['$scope', '$http', '$rootScope', 'LoginService','$window', loginCtrl])        

    function loginCtrl($scope, $http, $rootScope, LoginService, $window) {        
        
        $scope.init = function(){
             $scope.loginService = new LoginService($scope);
             $scope.loading = false;
             $rootScope.showHeader = false;
             $window.localStorage.removeItem('token');
        }

        $scope.verificaSeNaoFoiPreenchido = function(variavel){ 
                if(variavel == null || variavel == '')
                    return true
                
                return false
        }

        $scope.verificarDados = function(){ 
          
           $scope.empty_email = false;
           $scope.empty_pass = false;
           
           

           if($scope.verificaSeNaoFoiPreenchido($scope.email)){
                $scope.autorizaLogin = false;
                $scope.empty_email = true;
           }else if($scope.verificaSeNaoFoiPreenchido($scope.senha)){
                $scope.autorizaLogin = false;
                $scope.empty_pass = true;
           }else{
                $scope.autorizaLogin = true;
           }

           if($scope.autorizaLogin){ 
                $scope.login();
           }

        }

        $scope.login = function(){            
            $scope.loginJson = {                               
                                password: $scope.senha,
                                email: $scope.email

            }
            $scope.loading = true;

            $scope.loginService.loadUser($scope.loginJson, function (result){ 
                $scope.loading = false; 
                if(result.status != 200){
                    $scope.userNotFound = true;
                    return;
                }
                var user_token = result.data.data.token;
                $window.localStorage.setItem('token',user_token)
                window.location ="#/dashboard"

            });
            
        }      


        $scope.init();
        
    };


})(); 