(function () {
    'use strict';
    angular.module('app')
        .controller('pacienteDetailCtrl', ['$scope', '$http', '$rootScope', '$location','PacienteService', pacienteDetailCtrl])        

    function pacienteDetailCtrl($scope, $http, $rootScope, $location, PacienteService) {        
        
        $scope.init = function(){
            $scope.service = new PacienteService($scope);
            $scope.loadPacientes();         
           
        }

        $scope.loadPacientes = function(){ 
            $scope.service.getPacientes(function (result){  
                $scope.pacientes = result;
                $scope.getPaciente();
            })

           
            
        }

        $scope.getPaciente = function(){ 
           
            var url = $location.path(); 
            var id = url.substring(url.lastIndexOf('/') + 1);
         

            if(!isNaN(id)){          
                for(var i = 0; i < $scope.pacientes.length; i++){
                    if($scope.pacientes[i].id == id){
                        $scope.paciente = $scope.pacientes[i];
                    }
                }
               
            }
        
           
        }


        $scope.salvar = function(){
            alert('salvo')
        }

        

       
       

        $scope.init();
        
    };


})(); 