(function () {
    'use strict';
    angular.module('app')
        .controller('medicamentoDetailCtrl', ['$scope', '$http', '$rootScope', '$location','MedicamentoService', medicamentoDetailCtrl])        

    function medicamentoDetailCtrl($scope, $http, $rootScope, $location,MedicamentoService) {        
        
        $scope.init = function(){
            $scope.service = new MedicamentoService($scope);
            $scope.loadClientes();
          //  $scope.imageService = new ImageService($scope);
           
        }

        $scope.loadClientes = function(){

           $scope.service.getMedicamentos(function (result){ 
               $scope.medicamentos = result;
               $scope.getMedicamento();
            })

                    
        }



        $scope.getMedicamento = function(){
            var url = $location.path(); 
            var id = url.substring(url.lastIndexOf('/') + 1);
           

            if(!isNaN(id)){ 
            
                for(var i = 0; i < $scope.medicamentos.length; i++){
                    if($scope.medicamentos[i].id == id){
                         $scope.medicamentos[i].dose = parseInt($scope.medicamentos[i].dose);
                        $scope.medicamento = $scope.medicamentos[i];
                    }
                }
              
            }
        
            //alert(JSON.stringify($scope.cliente))
        }


        $scope.salvar = function(){
            alert(JSON.stringify($scope.medicamento))
        }

       
       

        $scope.init();
        
    };


})(); 