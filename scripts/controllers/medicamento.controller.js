(function () {
    'use strict';
    angular.module('app')
        .controller('medicamentoCtrl', ['$scope', '$http', '$rootScope', '$location','MedicamentoService', medicamentoCtrl])        

    function medicamentoCtrl($scope, $http, $rootScope, $location, MedicamentoService) {        
        
        $scope.init = function(){
            $scope.service = new MedicamentoService($scope);
            $scope.getMedicamentos();
          
           
        }

        $scope.getMedicamentos = function(){
            $scope.service.getMedicamentos(function (result){ 
               $scope.medicamentos= result;
            })
           
        }


        $scope.salvar = function(){
            alert('salvo')
        }

        $scope.goTo = function(medicamento) {
            $location.path('medicamento-detalhe/' + medicamento.id);
        };

       
       

        $scope.init();
        
    };


})(); 