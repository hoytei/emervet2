(function () {
    'use strict';
    angular.module('app')
        .controller('userDetailCtrl', ['$scope', '$http', '$rootScope', '$location', userDetailCtrl])        

    function userDetailCtrl($scope, $http, $rootScope, $location) {        
        
        $scope.init = function(){
            $scope.service = new ClienteService($scope);
            $scope.loadClientes();
          //  $scope.imageService = new ImageService($scope);
           
        }

        $scope.loadClientes = function(){

           $scope.service.getClientes(function (result){ 
               $scope.clientes = result;
               $scope.getCliente();
            })

                    
        }



        $scope.getCliente = function(){
            var url = $location.path(); 
            var id = url.substring(url.lastIndexOf('/') + 1);
            $scope.qtd_paciente = 0;

            if(!isNaN(id)){ 
            
                for(var i = 0; i < $scope.clientes.length; i++){
                    if($scope.clientes[i].id == id){
                        $scope.cliente = $scope.clientes[i];
                    }
                }
                $scope.qtd_paciente = $scope.cliente.pacientes.length;
            }
        
            //alert(JSON.stringify($scope.cliente))
        }


        $scope.salvar = function(){
            alert(JSON.stringify($scope.cliente))
        }

       
       

        $scope.init();
        
    };


})(); 