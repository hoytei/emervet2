(function () {
    'use strict';
    angular.module('app')
        .factory('PacienteService', ['$http', function ($http) {
        
            function PacienteService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            PacienteService.prototype.getPacientes = function (callback) { 
                var url = "http://demo6736551.mockable.io/get_pacientes";         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
             PacienteService.prototype.getClienteId = function (id, callback) { 
                var url = "http://demo4145276.mockable.io/imagens" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            PacienteService.prototype.save= function (json, callback) {
                var url = "http://demo0741009.mockable.io/save";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 
            return PacienteService;
        }])

}) (); 


