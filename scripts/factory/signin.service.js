(function () {
    'use strict';
    angular.module('app')
        .factory('SignInService', ['$http', function ($http) {
        
            function SignInService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            SignInService.prototype.save = function (json, callback) {
                var url = "http://localhost/projeto/back/look-do-dia/public/v1/users";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            };                       
            
            return SignInService;
        }])

}) (); 


