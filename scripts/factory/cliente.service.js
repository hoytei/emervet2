(function () {
    'use strict';
    angular.module('app')
        .factory('ClienteService', ['$http', function ($http) {
        
            function ClienteService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            ClienteService.prototype.getClientes = function (callback) { 
                var url = "http://demo6736551.mockable.io/get_clientes";         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
             ClienteService.prototype.getClienteId = function (id, callback) { 
                var url = "http://demo4145276.mockable.io/imagens" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            ClienteService.prototype.save= function (json, callback) {
                var url = "http://demo0741009.mockable.io/save";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 
            return ClienteService;
        }])

}) (); 


