(function () {
    'use strict';
    angular.module('app')
        .factory('TagsService', ['$http', function ($http) {
        
            function TagsService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            TagsService.prototype.getTags = function (callback) { 
                var token = localStorage.getItem('token');
                
                var url = "http://localhost/projeto/back/look-do-dia/public/v1/tags";         

                var req = {
                     method: 'GET',
                     url: 'http://localhost/projeto/back/look-do-dia/public/v1/tags',
                     headers: {
                       'token': token
                     }
                 }                 
                

                $http(req).then(function(response){
                        callback(response);
                });
                // $http.get(url,{cache: false}).success(function(response) {
                //     callback(response);
                // });      
            };                       
            
            
            return TagsService;
        }])

}) (); 


