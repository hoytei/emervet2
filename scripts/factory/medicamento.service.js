(function () {
    'use strict';
    angular.module('app')
        .factory('MedicamentoService', ['$http', function ($http) {
        
            function MedicamentoService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            MedicamentoService.prototype.getMedicamentos = function (callback) { 
                var url = "http://demo6736551.mockable.io/get_medicamentos";         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
             MedicamentoService.prototype.getMedicamentoId = function (id, callback) { 
                var url = "http://demo4145276.mockable.io/imagens" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            MedicamentoService.prototype.save= function (json, callback) {
                var url = "http://demo0741009.mockable.io/save";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 
            return MedicamentoService;
        }])

}) (); 


