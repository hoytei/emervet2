(function () {
    'use strict';
    angular.module('app')
        .factory('LoginService', ['$http', function ($http) {
        
            function LoginService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            LoginService.prototype.loadUser = function (json, callback) { 
                var url = "http://localhost/projeto/back/look-do-dia/public/v1/login";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) { 
                    callback(response);
                },function(response){ 
                    callback(response)
                });               
            };                       
            
            return LoginService;
        }])

}) (); 


